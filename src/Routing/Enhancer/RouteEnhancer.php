<?php

/**
 * @file
 * Contains \Drupal\route_skip_to_access\Routing\Enhancer\RouteEnhancer.
 */

namespace Drupal\route_skip_to_access\Routing\Enhancer;

use Drupal\Core\Routing\Enhancer\RouteEnhancerInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\Core\Access\AccessManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;
use Symfony\Cmf\Component\Routing\RouteObjectInterface;

/**
 * Skip current route if user does not have access.
 */
class RouteEnhancer implements RouteEnhancerInterface {

  /**
   * The route provider.
   * 
   * @var \Drupal\Core\Routing\RouteProviderInterface 
   */
  protected $routeProvider;

  /**
   * The access manager.
   *
   * @var \Drupal\Core\Access\AccessManagerInterface
   */
  protected $accessManager;

  /**
   * The current user service.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a SkipEnhancer object.
   */
  function __construct(RouteProviderInterface $route_provider, AccessManagerInterface $access_manager, AccountProxyInterface $current_user) {
    $this->routeProvider = $route_provider;
    $this->accessManager = $access_manager;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public function enhance(array $defaults, Request $request) {
    if (!$this->checkNamedRoute($defaults[RouteObjectInterface::ROUTE_NAME], $defaults)) {
      $parameters = $defaults['_raw_variables']->all();
      foreach ($this->routeProvider->getRouteCollectionForRequest($request) as $route_name => $route) {
        /* @var $route \Symfony\Component\Routing\Route */
        if ($route_name == $defaults[RouteObjectInterface::ROUTE_NAME]) {
          continue;
        }
        if ($this->checkNamedRoute($route_name, $parameters)) {
          return array_merge($defaults, $route->getDefaults(), array(
            RouteObjectInterface::ROUTE_NAME => $route_name,
            RouteObjectInterface::ROUTE_OBJECT => $route
          ));
        }
      }
    }
    return $defaults;
  }

  /**
   * Checks a named route with parameters against applicable access check services.
   *
   * Determines whether the route is accessible or not.
   *
   * @param string $route_name
   *   The route to check access to.
   * @param array $parameters
   *   Optional array of values to substitute into the route path pattern.
   *
   * @return bool
   */
  protected function checkNamedRoute($route_name, array $parameters = array()) {
    return $this->accessManager->checkNamedRoute($route_name, $parameters, $this->currentUser);
  }

  /**
   * {@inheritdoc}
   */
  public function applies(Route $route) {
    return ($route->hasOption('_skip_to_access') && $route->getOption('_skip_to_access'));
  }

}
