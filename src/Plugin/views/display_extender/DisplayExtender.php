<?php

/**
 * @file
 * Contains \Drupal\route_skip_to_access\Plugin\views\display_extender\DisplayExtender.
 */

namespace Drupal\route_skip_to_access\Plugin\views\display_extender;

use Drupal\views\Plugin\views\display_extender\DisplayExtenderPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\display\PathPluginBase;

/**
 * Display extender plugin.
 *
 * @ingroup views_display_extender_plugins
 *
 * @ViewsDisplayExtender(
 *   id = "route_skip_to_access",
 *   title = @Translation("Enable route skip to access")
 * )
 */
class DisplayExtender extends DisplayExtenderPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defineOptions() {
    $options = parent::defineOptions();
    if ($this->displayHandler instanceof PathPluginBase) {
      $options['skip'] = array(
        'default' => false
      );
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function optionsSummary(&$categories, &$options) {
    parent::optionsSummary($categories, $options);
    if ($this->displayHandler instanceof PathPluginBase) {
      $options['route_skip_to_access'] = array(
        'category' => 'page',
        'title' => $this->t('Route skip to access'),
        'value' => $this->getOption('skip') ? $this->t('Skip') : $this->t('No'),
        'desc' => $this->t('Set route skip to access')
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    if ($form_state->get('section') == 'route_skip_to_access' && $this->displayHandler instanceof PathPluginBase) {
      $form['#title'] .= $this->t('Route skip to access');
      $form['route_skip_to_access'] = array(
        '#title' => $this->t('Skip if not access to view to next view with current path'),
        '#type' => 'checkbox',
        '#default_value' => $this->getOption('skip', false)
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitOptionsForm(&$form, FormStateInterface $form_state) {
    parent::submitOptionsForm($form, $form_state);
    if ($form_state->get('section') == 'route_skip_to_access' && $this->displayHandler instanceof PathPluginBase) {
      $this->setOption('skip', $form_state->getValue('route_skip_to_access', false) ? true : false);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getOption($name, $default = null) {
    return array_key_exists($name, $this->options) ? $this->options[$name] : $default;
  }

  /**
   * {@inheritdoc}
   */
  protected function setOption($name, $value) {
    $this->options[$name] = $value;
    return $this;
  }

}
