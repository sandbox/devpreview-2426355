<?php

/**
 * @file
 * Contains \Drupal\route_skip_to_access\EventSubscriber\RouteSubscriber.
 */

namespace Drupal\route_skip_to_access\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;
use Drupal\views\Entity\View as ViewEntity;
use Drupal\views\Views;

/**
 * Builds up the routes of all views.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if (array_key_exists('route_skip_to_access', Views::getEnabledDisplayExtenders())) {
      foreach ($collection->all() as $name => $route) {
        /* @var $route \Symfony\Component\Routing\Route */
        if ($route->hasDefault('view_id') && $route->hasDefault('display_id') && $name == 'view.' . $route->getDefault('view_id') . '.' . $route->getDefault('display_id')) {
          $view = ViewEntity::load($route->getDefault('view_id'));
          $display = $view->getDisplay($route->getDefault('display_id'));
          $display_extenders = $display['display_options']['display_extenders'];
          if (array_key_exists('route_skip_to_access', $display_extenders) && $display_extenders['route_skip_to_access']['skip'] === true) {
            $route->setOption('_skip_to_access', true);
          }
        }
      }
    }
  }

}
